from pyworks import Task

class PinitTask( Task ):
    def conf( self ):
        self.ws = self.get_service( "ws" )
        
    def setpin( self, street, city, country="Denmark" ):
        arg = "address:%s,%s,%s" % ( street, city, country )
        print "setpin: %s" % ( arg )
        self.ws.send( arg )
        
    
