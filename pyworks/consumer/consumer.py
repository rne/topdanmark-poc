from urlparse import urlparse, parse_qs
from threading import Thread
from pyworks import Task
from stompest.config import StompConfig
from stompest.protocol import StompSpec
from stompest.sync import Stomp

CONFIG = StompConfig('tcp://localhost:61613')
QUEUE = '/topic/test'

class ConsumerTask( Task ):
    def init( self ):
        self.client = Stomp(CONFIG)
        self.client.connect()
        self.client.subscribe(QUEUE, {StompSpec.ACK_HEADER: StompSpec.ACK_CLIENT_INDIVIDUAL})

    def conf( self ):
        self.stop = False
        self.t = Thread( target=self.consume )
        self.t.setDaemon( True )
        self.t.start( )
        self.pinit = self.get_service( "pinit" )
        
    def consume( self ):
        while self.stop == False :
            frame = self.client.receiveFrame()
            query = frame.body
            print( 'Got %s' % query )
            r = parse_qs( query )
            if len( r ) < 2 :
                print "Got empty frame"
                continue
            street = r['street'][0]
            city = r['city'][0]
            country = r['country'][0]
            self.pinit.setpin( street, city, country )
            self.client.ack(frame)
            
    def close( self ):
        if self.stop == False and self.t != None:
            self.stop = True
            time.sleep( 1 )
    
        
